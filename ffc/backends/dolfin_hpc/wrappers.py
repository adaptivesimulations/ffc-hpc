# -*- coding: utf-8 -*-
# Copyright (C) 2011 Marie E. Rognes
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
#
# Based on original implementation by Martin Alnes and Anders Logg

from . import includes as incl
from .functionspace import *
from .form import generate_form, generate_coefficient_map_data_hpc
from .capsules import UFCElementNames

__all__ = ["generate_dolfin_hpc_code"]

# NB: generate_dolfin_namespace(...) assumes that if a coefficient has
# the same name in multiple forms, it is indeed the same coefficient:
parameters = {"use_common_coefficient_names": True}

def comp_type(forms, i):
    ranks = [form.rank for form in forms]
    count = [ranks.count(0), ranks.count(1), ranks.count(2)]
    if len(ranks) <= 3 and sum(count) > 0 and min(count) >= 0 and max(count) <= 1:
        postfixes = ["Functional", "LinearForm", "BilinearForm"]
    return postfixes[ranks[i]]

def comp_prefix(prefix, forms, i):
    ranks = [form.rank for form in forms]
    count = [ranks.count(0), ranks.count(1), ranks.count(2)]
    if len(ranks) <= 3 and sum(count) > 0 and min(count) >= 0 and max(count) <= 1:
        postfixes = ["Functional", "LinearForm", "BilinearForm"]
        return "%s%s" % (prefix, postfixes[ranks[i]])
        # Return prefix_i if we have more than one rank
    if len(ranks) > 1:
        return "%s_%d" % (prefix, i)
    # Else, just return prefix
    return prefix

def generate_backward_compatible_code(prefix, forms):

    output = """\
// DOLFIN wrappers
#include <dolfin/fem/CoefficientMap.h>
"""

    form_class_template_hpc = """\

class %(form_type)s : public dolfin::%(form_type)s
{
public:

  %(form_type)s(%(constructor_args)s) : dolfin::%(form_type)s(%(base_args)s)
  {
%(constructor_body)s
    Form::init(coefficients_);
  }

  %(form_type)s(dolfin::Mesh& mesh, dolfin::CoefficientMap& map) : dolfin::%(form_type)s(mesh)
  {
    Form::init(coefficients_, map);
  }

  /// Return UFC form
  ufc::form const& form() const
  {
    return form_;
  }

  /// Return vector of coefficients
  std::vector<dolfin::Coefficient*> const& coefficients() const
  {
    return coefficients_;
  }

  /// Return the name of the coefficient with this number
  std::string coefficient_name(dolfin::size_t const i) const
  {
%(coefficient_name)s
  }

private:

  // UFC form
  %(class_prefix)s_form_%(class_suffix)s form_;

  /// vector of coefficients
  std::vector<dolfin::Coefficient*> coefficients_;

};

"""

    form_map_ctor_body = """\
    Form::assign_coefficients(coefficient_map, coefficients_);
"""

    space_namespace_tpl = """\
    namespace %(prefix)sSpaces
{
    %(code)s
}
"""

    space_function_tpl = """\
  std::string const& %(space_name)s()
  {
     static std::string signature("%(space_signature)r");
     return signature;
  }

"""

    for i in range(len(forms)):
        form_type = comp_type(forms, i)
        output+= "#include <dolfin/fem/%s.h>\n" %  (form_type)

    output+= "struct %s\n{\n" % (prefix)

    # Generate code for common coefficient spaces
#    spaces = extract_coefficient_spaces(forms)
#    spacecode = ""
#    for space in spaces:
#        spacecode+= space_function_tpl % { 'space_name' : space[0] , 'space_signature' : space[1] }
#    output += space_namespace_tpl % { 'prefix' : prefix, 'code' : spacecode }

    for i in range(len(forms)):
        form_prefix = comp_prefix(prefix, forms, i)
        form_type = comp_type(forms, i)
        (coefficient_index, coefficient_name) = generate_coefficient_map_data_hpc(forms[i])
        constructor_args = "dolfin::Mesh& mesh"
        if forms[i].num_coefficients > 0:
              constructor_args += ", "+", ".join(["dolfin::Coefficient& w%d" % j for j in range(forms[i].num_coefficients)])
        base_args = "mesh"
        constructor_body = "\n".join(["    coefficients_.push_back(&w%d);" % j for j in range(forms[i].num_coefficients)])
        args = {"form_prefix": form_prefix,
        "form_type": form_type,
        "base_args": base_args,
        "constructor_args": constructor_args,
        "constructor_body": constructor_body,
        "coefficient_index": coefficient_index,
        "coefficient_name": coefficient_name,
        "class_prefix": prefix.lower(),
        "class_suffix": str(i)}
        output+= form_class_template_hpc % args

    output+= "\n};\n"
    return [output]


def generate_dolfin_hpc_code(prefix, header, forms,
                             common_function_space=False, add_guards=False,
                             error_control=False):
    """Generate complete dolfin wrapper code with given generated names.

    @param prefix:
        String, prefix for all form names.
    @param header:
        Code that will be inserted at the top of the file.
    @param forms:
        List of UFCFormNames instances or single UFCElementNames.
    @param common_function_space:
        True if common function space, otherwise False
    @param add_guards:
        True iff guards (ifdefs) should be added
    @param error_control:
        True iff adaptivity typedefs (ifdefs) should be added
    """

    # Generate dolfin namespace
    #namespace = generate_dolfin_namespace(prefix, forms, common_function_space,
    #                                      error_control)

    # Collect pieces of code
    #code = [incl.dolfin_tag, header, incl.stl_includes, incl.dolfin_includes,
    #        namespace]

    #code = ["#ifndef UFC_BACKWARD_COMPATIBILITY \n"] + code
    code = ["/// Code generated with DOLFIN-HPC 0.9.3 wrappers."]
    # Add ifdefs/endifs if specified
    if add_guards:
        guard_name = ("%s_h" % prefix).upper()
        preguard = "#ifndef %s\n#define %s\n" % (guard_name, guard_name)
        postguard = "\n#endif\n\n"
        code = [preguard] + code + [postguard]

    #code = code + ["#else \n"]
    code = code + generate_backward_compatible_code(prefix, forms)
    #code = code + ["#endif \n"]
    # Return code
    return "\n".join(code)


def generate_dolfin_namespace(prefix, forms, common_function_space=False,
                              error_control=False):

    # Allow forms to represent a single space, and treat separately
    if isinstance(forms, UFCElementNames):
        return generate_single_function_space(prefix, forms)

    # Extract (common) coefficient spaces
    assert(parameters["use_common_coefficient_names"])
    spaces = extract_coefficient_spaces(forms)

    # Generate code for common coefficient spaces
    code = [apply_function_space_template(*space) for space in spaces]

    # Generate code for forms
    code += [generate_form(form, "Form_%s" % form.name) for form in forms]

    # Generate namespace typedefs (Bilinear/Linear & Test/Trial/Function)
    code += [generate_namespace_typedefs(forms, common_function_space,
                                         error_control)]

    # Wrap code in namespace block
    code = "\nnamespace %s\n{\n\n%s\n}" % (prefix, "\n".join(code))

    # Return code
    return code


def generate_single_function_space(prefix, space):
    code = apply_function_space_template("FunctionSpace",
                                         space.ufc_finite_element_classnames[0],
                                         space.ufc_dofmap_classnames[0])
    code = "\nnamespace %s\n{\n\n%s\n}" % (prefix, code)
    return code


def generate_namespace_typedefs(forms, common_function_space, error_control):

    # Generate typedefs as (fro, to) pairs of strings
    pairs = []

    # Add typedef for Functional/LinearForm/BilinearForm if only one
    # is present of each
    aliases = ["Functional", "LinearForm", "BilinearForm"]
    extra_aliases = {"LinearForm": "ResidualForm", "BilinearForm": "JacobianForm"}
    for rank in sorted(list(range(len(aliases))), reverse=True):
        forms_of_rank = [form for form in forms if form.rank == rank]
        if len(forms_of_rank) == 1:
            pairs += [("Form_%s" % forms_of_rank[0].name, aliases[rank])]
            if aliases[rank] in extra_aliases:
                extra_alias = extra_aliases[aliases[rank]]
                pairs += [("Form_%s" % forms_of_rank[0].name, extra_alias)]

    # Keepin' it simple: Add typedef for FunctionSpace if term applies
    if common_function_space and any(form.rank for form in forms):
        pairs += [("Form_%s::TestSpace" % forms[0].name, "FunctionSpace")]

    # Add specialized typedefs when adding error control wrapppers
    if error_control:
        pairs += error_control_pairs()

    # Combine data to typedef code
    typedefs = "\n".join("typedef %s %s;" % (to, fro) for (to, fro) in pairs)

    # Return typedefs or ""
    if not typedefs:
        return ""
    return "// Class typedefs\n" + typedefs + "\n"


def error_control_pairs():
    return [("Form_8", "BilinearForm"), ("Form_9", "LinearForm"),
            ("Form_10", "GoalFunctional")]

